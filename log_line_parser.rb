class LogLineParser
  def initialize(line)
    @line = line
  end
  def message
    ["[ERROR]: ","[WARNING]: ","[INFO]: "].each do |prefix| 
      return @line.delete_prefix(prefix).strip if @line.include? prefix 
    end
  end
  def log_level
    ["[ERROR]:","[WARNING]:","[INFO]:"].each do |prefix| 
      return prefix.downcase[1, prefix.length - 3] if @line.include? prefix 
    end
  end
  def reformat
    "#{message} (#{log_level})"
  end
end

LogLineParser.new('[ERROR]: Invalid operation').message
LogLineParser.new("[WARNING]:  Disk almost full\r\n").message
LogLineParser.new('[ERROR]: Invalid operation').log_level
LogLineParser.new('[INFO]: Operation completed').reformat
