
class Calculate
  
  begin
      puts 'First number:'
      a = $stdin.gets.chomp.to_i
    
      puts 'Second number:'
      b = $stdin.gets.chomp.to_i
    
      operation = nil
      unless ['+', '-', '*', '/', '**'].include?(operation)
        puts 'Choose operation:  (+ - * /):'
        operation = $stdin.gets.chomp
      end
    
      result = nil
      success = false
    
      case operation
      when '+'
        result = (a + b).to_s
      when '-'
        result = (a - b).to_s
      when '*'
        result = (a * b).to_s
      when '/'
        result = (a / b).to_s
      when '**'
        result = (a**b).to_s
      else
        puts 'There is not such kind of operation'
      end
      success = true
      puts "Le résultat est: #{result}"
    rescue ZeroDivisionError => e
      puts "You tried to devide number by zero! Error: #{e.message}"
    end
    
    if success
      puts "\nSuccess!"
    else
      puts "\nSomething goes wrong :("
    end
end

