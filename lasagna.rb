class Lasagna
  EXPECTED_MINUTES_IN_OVEN  = 40
  def remaining_minutes_in_oven(actual_minutes_in_oven)
    expected = 40
    remaining = expected - actual_minutes_in_oven
  end
  def preparation_time_in_minutes(layers)
    preparation_time = layers*2
  end
  def total_time_in_minutes(number_of_layers:, actual_minutes_in_oven:)
    total_time = ((number_of_layers*2) + actual_minutes_in_oven)
  end
end
# Créer un nouvel objet
lasagna = Lasagna.new
puts lasagna.remaining_minutes_in_oven(40)
puts lasagna.preparation_time_in_minutes(2)
puts lasagna.total_time_in_minutes(number_of_layers: 3, actual_minutes_in_oven: 20)


